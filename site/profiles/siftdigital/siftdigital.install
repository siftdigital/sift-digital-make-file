<?php

/**
 * @file
 * Implements hook_install().
 * Perform actions to set up the site for this profile.
 */
function siftdigital_install() {
  _siftdigital_text_formats();
  _siftdigital_content_types();
  _siftdigital_permissions();
  _siftdigital_themes();
  _siftdigital_wysiwyg();

  // Allow visitor account creation with administrative approval.
  variable_set('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL);

  // set first day of the week to 'Monday'
  variable_set('date_first_day', '1');

  // default content URL alias should mirror the menu system
  variable_set('pathauto_node_pattern', '[node:menu-link:parent:url:path]/[node:menu-link:title]');
}

function _siftdigital_text_formats() {
  // Add text formats.
  $full_html_format = array(
      'format' => 'full_html',
      'name' => 'Full HTML',
      'weight' => 0,
      'filters' => array(
          // URL filter.
          'filter_url' => array(
              'weight' => 0,
              'status' => 1,
          ),
          // Line break filter.
          'filter_autop' => array(
              'weight' => 1,
              'status' => 1,
          ),
          // Image resize filter
          'image_resize_filter' => array(
              'weight' => 3,
              'status' => 1,
          ),
          // HTML corrector filter.
          'filter_htmlcorrector' => array(
              'weight' => 10,
              'status' => 1,
          ),
      ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);

  $filtered_html_format = array(
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
      'weight' => 1,
      'filters' => array(
          // URL filter.
          'filter_url' => array(
              'weight' => 0,
              'status' => 1,
          ),
          // HTML filter.
          'filter_html' => array(
              'weight' => 1,
              'status' => 1,
          ),
          // Line break filter.
          'filter_autop' => array(
              'weight' => 2,
              'status' => 1,
          ),
          // Image resize filter
          'image_resize_filter' => array(
              'weight' => 3,
              'status' => 1,
          ),
          // HTML corrector filter.
          'filter_htmlcorrector' => array(
              'weight' => 10,
              'status' => 1,
          ),
      ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);
}

function _siftdigital_content_types() {
  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
      array(
          'type' => 'page',
          'name' => st('Page'),
          'base' => 'node_content',
          'description' => st("Use <em>pages</em> for your static content, such as an 'About us' page."),
          'custom' => 1,
          'modified' => 1,
          'locked' => 0,
      ),
  );
  foreach ($types as $type) {
      $type = node_type_set_defaults($type);
      node_type_save($type);
      node_add_body_field($type);
  }

  // Insert default pre-defined RDF mapping into the database.
  $rdf_mappings = array(
      array(
          'type' => 'node',
          'bundle' => 'page',
          'mapping' => array(
              'rdftype' => array('foaf:Document'),
          ),
      ),
  );
  foreach ($rdf_mappings as $rdf_mapping) {
      rdf_mapping_save($rdf_mapping);
  }

  // Default "Page" to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);
}

function _siftdigital_permissions() {
  // Basic permissions for system roles.
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content'));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content'));

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'admin';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "admin" role.
  db_insert('users_roles')
      ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
      ->execute();
}

function _siftdigital_themes() {
  // Enable the admin theme.
  db_update('system')
      ->fields(array('status' => 1))
      ->condition('type', 'theme')
      ->condition('name', 'seven')
      ->execute();
  variable_set('admin_theme', 'seven');
  variable_set('node_admin_theme', '1');
}


/**
 *    Configure simple WYSIWYG profiles and a default linkit profile
 */

function _siftdigital_wysiwyg() {

$filtered_html_wysiwyg = array(
  'default' => 1,
  'user_choose' => 0,
  'show_toggle' => 1,
  'theme' => 'advanced',
  'language' => 'en',
  'buttons' =>
  array (
    'default' =>
    array (
      'Bold' => 1,
      'Italic' => 1,
      'Underline' => 1,
//      'Strike' => 0,
//      'JustifyLeft' => 0,
//      'JustifyCenter' => 0,
//      'JustifyRight' => 0,
//      'JustifyBlock' => 0,
      'BulletedList' => 1,
      'NumberedList' => 1,
      'Outdent' => 1,
      'Indent' => 1,
      'Undo' => 1,
      'Redo' => 1,
//      'Link' => 0,
//      'Unlink' => 0,
       'Anchor' => 1,
//      'Image' => 0,
//      'TextColor' => 0,
//      'BGColor' => 0,
//      'Superscript' => 0,
//      'Subscript' => 0,
      'Blockquote' => 1,
//      'Source' => 0,
//      'HorizontalRule' => 0,
      'Cut' => 1,
      'Copy' => 1,
      'Paste' => 1,
//      'PasteText' => 0,
      'PasteFromWord' => 1,
//      'ShowBlocks' => 0,
      'RemoveFormat' => 1,
//      'SpecialChar' => 0,
//      'Format' => 0,
//      'Font' => 0,
//      'FontSize' => 0,
//      'Styles' => 0,
//      'Table' => 0,
//      'SelectAll' => 0,
//      'Find' => 0,
//      'Replace' => 0,
//      'Flash' => 0,
//      'Smiley' => 0,
//      'CreateDiv' => 0,
//      'Iframe' => 0,
//      'Maximize' => 0,
      'SpellChecker' => 1,
//      'Scayt' => 0,
 //     'About' => 0,
    ),
    'linkit' =>
    array (
      'linkit' => 1,
    ),
    'drupal' =>
    array (
      'break' => 1,
    ),
  ),
  'toolbar_loc' => 'top',
  'toolbar_align' => 'left',
  'path_loc' => 'bottom',
  'resizing' => 1,
  'verify_html' => 1,
  'preformatted' => 0,
  'convert_fonts_to_spans' => 1,
  'remove_linebreaks' => 1,
  'apply_source_formatting' => 0,
  'paste_auto_cleanup_on_paste' => 0,
  'block_formats' => 'p,h2,h3,h4',
  'css_setting' => 'theme',
  'css_path' => '',
  'css_classes' => '',
  );

  $full_html_wysiwyg = array(
  'default' => 1,
  'user_choose' => 0,
  'show_toggle' => 1,
  'theme' => 'advanced',
  'language' => 'en',
  'buttons' =>
  array (
    'default' =>
    array (
      'Bold' => 1,
      'Italic' => 1,
      'Underline' => 1,
//      'Strike' => 0,
//      'JustifyLeft' => 0,
//      'JustifyCenter' => 0,
//      'JustifyRight' => 0,
//      'JustifyBlock' => 0,
      'BulletedList' => 1,
      'NumberedList' => 1,
      'Outdent' => 1,
      'Indent' => 1,
      'Undo' => 1,
      'Redo' => 1,
//      'Link' => 0,
//      'Unlink' => 0,
      'Anchor' => 1,
//      'Image' => 0,
//      'TextColor' => 0,
//      'BGColor' => 0,
//      'Superscript' => 0,
//      'Subscript' => 0,
      'Blockquote' => 1,
//      'Source' => 0,
//      'HorizontalRule' => 0,
      'Cut' => 1,
      'Copy' => 1,
      'Paste' => 1,
//      'PasteText' => 0,
      'PasteFromWord' => 1,
//      'ShowBlocks' => 0,
      'RemoveFormat' => 1,
//      'SpecialChar' => 0,
      'Format' => 1,
//      'Font' => 0,
//      'FontSize' => 0,
//      'Styles' => 0,
//      'Table' => 0,
//      'SelectAll' => 0,
      'Find' => 1,
      'Replace' => 1,
//      'Flash' => 0,
//      'Smiley' => 0,
//      'CreateDiv' => 0,
//      'Iframe' => 0,
//      'Maximize' => 0,
      'SpellChecker' => 1,
//      'Scayt' => 0,
 //     'About' => 0,
    ),
    'linkit' =>
    array (
      'linkit' => 1,
    ),
    'drupal' =>
    array (
      'break' => 1,
    ),
  ),
  'toolbar_loc' => 'top',
  'toolbar_align' => 'left',
  'path_loc' => 'bottom',
  'resizing' => 1,
  'verify_html' => 1,
  'preformatted' => 0,
  'convert_fonts_to_spans' => 1,
  'remove_linebreaks' => 1,
  'apply_source_formatting' => 0,
  'paste_auto_cleanup_on_paste' => 0,
  'block_formats' => 'p,pre,h3,h4,h5,h6,address,pre,code,dt,dd',
  'css_setting' => 'theme',
  'css_path' => '',
  'css_classes' => '',
  );

  $wysiwyg = db_insert('wysiwyg')->fields(array(
    'format' => 'full_html',
    'editor' => 'ckeditor',
    'settings' => serialize($full_html_wysiwyg)
  ))
  ->execute();

  $wysiwyg = db_insert('wysiwyg')->fields(array(
    'format' => 'filtered_html',
    'editor' => 'ckeditor',
    'settings' => serialize($filtered_html_wysiwyg)
  ))
  ->execute();

  $wysiwyg = db_insert('wysiwyg')->fields(array(
    'format' => 'plain_text',
    'editor' => '',
    'settings' => NULL
  ))
  ->execute();


  // Linkit profile....
  $linkit_profile = new stdClass();
  $linkit_profile->name = 'default_profile';
  $linkit_profile->admin_title = 'Default profile';
  $linkit_profile->admin_description = 'Linkit editor default profile';
  $linkit_profile->profile_type = '1';
  $linkit_profile->data = array(
    'text_formats' => array(
      'full_html' => 'full_html',
      'filtered_html' => 0,
      'plain_text' => 0,
    ),
    'search_plugins' => array(
      'entity:taxonomy_term' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:field_collection_item' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:comment' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:node' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'entity:user' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'entity:comment' => array(
      'result_description' => '',
      'bundles' => array(
        'comment_node_page' => 0,
        'comment_node_webform' => 0,
      ),
      'group_by_bundle' => 0,
    ),
    'entity:field_collection_item' => array(
      'result_description' => '',
    ),
    'entity:node' => array(
      'result_description' => '',
      'bundles' => array(
        'page' => 0,
        'webform' => 0,
      ),
      'group_by_bundle' => 0,
      'include_unpublished' => 0,
    ),
    'entity:taxonomy_term' => array(
      'result_description' => '',
    ),
    'entity:user' => array(
      'result_description' => '',
    ),
    'insert_plugin' => array(
      'url_method' => '2',
    ),
    'attribute_plugins' => array(
      'rel' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'class' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'accesskey' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'target' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'title' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'id' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'autocomplete' => array(
      'charLimit' => '3',
      'wait' => '350',
      'remoteTimeout' => '10000',
    ),
  );

  $linkit = db_insert('linkit_profiles')->fields(array(
    'name' => $linkit_profile->name,
    'admin_title' => $linkit_profile->admin_title,
    'admin_description' => $linkit_profile->admin_description,
    'data' => serialize($linkit_profile->data),
    'profile_type' => $linkit_profile->profile_type,
  ))
  ->execute();

}
