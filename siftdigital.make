; USAGE
; ------------
;
; drush make siftdigital.make site
;

; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.

core = 7.x

; API version
; ------------
; Every makefile needs to declare it's Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------

projects[] = drupal

; INSTALL PROFILES
; --------

projects[siftdigital][type] = "profile"
projects[siftdigital][download][type] = "git"
projects[siftdigital][download][url] = "https://bitbucket.org/siftdigital/awesome-profile.git"
projects[siftdigital][download][branch] = "master"

; COMMON
; --------

projects[admin_menu][subdir] = "contrib"
projects[better_exposed_filters][subdir] = "contrib"
projects[context][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[elysia_cron][subdir] = "contrib"
projects[email][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[entity_view_mode][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[field_collection][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[globalredirect][subdir] = "contrib"
projects[google_tag][subdir] = "contrib"
projects[image_link_formatter][subdir] = "contrib"
projects[jquery_update][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[link][subdir] = "contrib"
projects[memcache][subdir] = "contrib"
projects[menu_block][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[nodequeue][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[rabbit_hole][subdir] = "contrib"
projects[redirect][subdir] = "contrib"
projects[roleassign][subdir] = "contrib"
projects[rules][subdir] = "contrib"
projects[smtp][subdir] = "contrib"
projects[strongarm][subdir] = "contrib"
projects[title][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[transliteration][subdir] = "contrib"
projects[views][subdir] = "contrib"
projects[webform][subdir] = "contrib"
projects[taxonomy_manager][subdir] = "contrib"
projects[paragraphs][subdir] = "contrib"
projects[flag][subdir] = "contrib"
projects[search_api][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[navbar][subdir] = "contrib"
projects[workbench][subdir] = "contrib"
projects[workbench_moderation][subdir] = "contrib"
projects[workbench_scheduler][subdir] = "contrib"
projects[chosen][subdir] = "contrib"
projects[mollom][subdir] = "contrib"
projects[spambot][subdir] = "contrib"
projects[ckeditor][subdir] = "contrib"
projects[webform_protected_downloads][subdir] = "contrib"
projects[download_count][subdir] = "contrib"
projects[views_data_export][subdir] = "contrib"
projects[radioactivity][subdir] = "contrib"
projects[login_history][subdir] = "contrib"
projects[hotjar][subdir] = "contrib"
projects[optimizely][subdir] = "contrib"
projects[addthis][subdir] = "contrib"
projects[advagg][subdir] = "contrib"
projects[imageapi_optimize][subdir] = "contrib"
projects[adminrole][subdir] = "contrib"
projects[features_orphans][subdir] = "contrib"
projects[ftools][subdir] = "contrib"
projects[features_extra][subdir] = "contrib"
projects[nodequeue][subdir] = "contrib"

; DEV
; --------

projects[devel][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[search_krumo][subdir] = "contrib"

; PATCHED
; --------

; WYSIWYG
; --------

projects[emptyparagraphkiller][subdir] = "contrib"
projects[linkit][subdir] = "contrib"
projects[linkit_views][subdir] = "contrib"
projects[linkit_views][version] = 2.x-dev
projects[pathologic][subdir] = "contrib"
projects[ckeditor][subdir] = "contrib"

; LIBRARIES
; --------

; ensure latest version of ckeditor....
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "http://github.com/ckeditor/ckeditor-releases.git"
libraries[ckeditor][download][branch] = "full/stable"

; BETTER ADMINISTRATION EXPERIENCE
; --------------------------------

projects[admin_views][subdir] = "contrib"
projects[context_admin][subdir] = "contrib"
projects[views_bulk_operations][subdir] = "contrib"

; SEO
; ---

projects[metatag][subdir] = "contrib"
projects[xmlsitemap][subdir] = "contrib"

; SIGN IN AND REGISTRATION
; ------------------------

projects[betterlogin][subdir] = "contrib"
projects[remember_me][subdir] = "contrib"



; MEDIA 2
; Uncomment the following lines to download the Media 2 modules
; -------

projects[file_entity][subdir] = "contrib"
projects[media][subdir] = "contrib"
projects[media_browser_plus][subdir] = "contrib"
projects[media_vimeo][subdir] = "contrib"
projects[media_youtube][subdir] = "contrib"

projects[epsacrop][subdir] = "contrib"
