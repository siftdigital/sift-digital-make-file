Sift Digital Make File
=============

Clone the make file into a local project folder:

`git clone git@bitbucket.org:siftdigital/sift-digital-make-file.git my_project`

cd into the project folder and run:

`drush make siftdigital.make site`

**If using Rundeck...**

Deploy using Rundeck interface. Anisble config files will need to be correct for the project, so check this if necessary (e.g. if a new project)

**If using old deploy tools...**

Clone build tools into project root

`git clone git@bitbucket.org:siftdigital/deploy.git deploy`

Edit prop XML files as necessary, then deploy using

`sudo ant deploy.devel`

**Run the site installation...** 

...Using the siftdigital profile, either via the web interface (/install.php), or via drush:

`drush si siftdigital`

For more info about how to configure the site install visit this [page][1]
[1]: http://www.drush.org/#site-install

### Detailed Instructions ###
```
cd /data/drupal7
git clone git@bitbucket.org:siftdigital/sift-digital-make-file.git <PROJECT NAME>
git clone git@bitbucket.org:siftdigital/deploy.git deploy
```
* Remove .git folders from both repos
* Edit make file - comment out unnecessary modules
```
drush make siftdigital.make site
```
* Ensure site/profiles/siftdigital/siftdigital.profile dependencies match make file (modules removed from make file must be commented out here too).

* In browser - http://<alias>.office.sift.com/install.php
* Select 'Sift Digital' profile
* Install site (NB: I had to increase php.ini script execution time from 60s to 90s to allow time for all tables to be created).
* Fill in site settings form

* Log into site - check working
* Create bitbucket repo for project and push